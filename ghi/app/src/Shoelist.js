function App(props) {
    if (props.hats === undefined) {
      return "gabagool";
    }
    return (
      <div className="container">
          <table className="table table-striped">
              <thead>
                  <tr>
                      <th>Coloe</th>
                      <th>Type</th>
                  </tr>
              </thead>
              <tbody>
                  {props.hats.map(hats => {
                      return (
                          <tr key={hats.href}>
                              <td>{ hats.color }</td>
                              <td>{ hats.type }</td>
                          </tr>
                      );
                  })}
              </tbody>
          </table>
      </div>
    );
  }

  export default App;