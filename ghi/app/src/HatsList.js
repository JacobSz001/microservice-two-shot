function App(props) {
    if (props.hats === undefined) {
        return null;
    }
    return (
      <div className="container">
          <table className="table table-striped">
              <thead>
                  <tr>
                      <th>Hat List</th>
                  </tr>
              </thead>
              <tbody>
                  {props.hats.map(hats => {
                      return (
                          <tr key={hats.href}>
                              <td>
                                <img src={ hats.picture_url } alt="Hat" width="100" height="100"/>
                              </td>
                          </tr>
                      );
                  })}
              </tbody>
          </table>
      </div>
    );
  }

  export default App;