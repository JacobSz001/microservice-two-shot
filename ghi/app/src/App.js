import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatForm from './HatForm';
{/*import ShoeForm from './ShoeForm';*/}

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <HatsList hats={props.hats} />
        <HatForm />
        <Routes>
          <Route path="/" element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
