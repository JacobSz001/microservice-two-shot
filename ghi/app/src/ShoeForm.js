class HatForm extends React.Component {
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Register New Hat</h1>
              <form id="create-hat-form">
                <div className="form-floating mb-3">
                  <input placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input placeholder="Style" required type="text"  name="style" id="style" className="form-control"/>
                  <label htmlFor="style">Style</label>
                </div>
                <div className="form-floating mb-3">
                  <input placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                  <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                  <select required name="closet" id="closet" className="form-select">
                    <option value="">Choose a closet</option>
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

  export default ShoeForm;