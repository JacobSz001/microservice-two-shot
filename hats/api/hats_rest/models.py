from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=200)
    import_href = models.CharField(max_length=200, unique=True)


class Hat(models.Model):
    fabric = models.CharField(max_length=50)
    style = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField()

    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return self.picture_url

    class Meta:
        ordering = ("picture_url",)